
# coding: utf-8

# In[1]:


## data processsing
import pandas as pd
import numpy as np
import re
import pickle
#################################### 
############ file path #############
#################################### 


# # data loading and preprocessing
# 

# In[19]:
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import *

import json
with open('data/Headline_Trainingdata.json') as f:
    data = json.load(f)

df_raw = pd.DataFrame(data=data)
df_raw.sample(frac=1)
company_name = df_raw["company"].tolist()
X_raw = df_raw["title"].tolist()
y_raw = df_raw["sentiment"].tolist()
X_raw = [twit.replace(company_name[i],  '') for i, twit in enumerate(X_raw)] 


# In[3]:


import nltk
def remove_stopwords(X):
    word_tokens = word_tokenize(X)
    stop_words = pd.read_csv("data/stop_words.csv")["stop word"].tolist()
    X = [w for w in word_tokens if not w in stop_words]
    X = ' '.join(X)
    return X
        
def stemming(X):
    stemmer = PorterStemmer()
    word_tokens = word_tokenize(X)
    X = [stemmer.stem(w) for w in word_tokens]
    X = ' '.join(X)
    return X

def remove_noun(X):
    X = nltk.tag.pos_tag(X.split())
    X = [word for word, tag in X if tag != 'NNP' and tag != 'NNPS' and tag != 'NN']
    X = ' '.join(X)
    return X

def cleasing(X):    
    X = [re.sub(r'[^\x00-\x7F]+','', twit) for twit in X]
    X = [re.sub(r"^\d+\s|\s\d+\s|\s\d+$",'', twit) for twit in X]
    X = [re.sub(r'http\S+', '', twit) for twit in X]
    X = [re.sub(r'#', '', twit) for twit in X]
    X = [re.sub(r'@', '', twit) for twit in X]
    X = [twit.lower() for twit in X]
    X = [remove_stopwords(twit) for twit in X]
    return X


# # feature engineering

# In[4]:
from nltk import ngrams
import numpy
from keras.datasets import imdb
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers.embeddings import Embedding
from keras.preprocessing import sequence
import nltk
from nltk import word_tokenize
numpy.random.seed(7)


# In[5]:
def two_grams(twit):
    n = 2
    tokens = ngrams(twit.split(), n)
    X = [token for token in tokens]
    return X
def unit_grams(twit):
    n = 1
    tokens = ngrams(twit.split(), n)
    X = [token for token in tokens]
    return X
def ngrams_features(X):
    X_1 = [unit_grams(twit) for twit in X] 
    X_2 = [two_grams(twit) for twit in X]
    return X_1, X_2

# In[6]:
def find_dict(X, top_words):
    ### use NLTK ######
    # Tokenize
    # every element in X will be transfered to a feature vector, 
    # each element in the feature vector represents the occrence of a word
    # the dimenson of the vector equals to the top_word.
    all_words = []
    for words in X:
        all_words = all_words + words
    # frequency and pick the top 5000 features
    all_words = nltk.FreqDist(all_words)
    print("the size of the vocab is: ", len(all_words))
    word_features = list(all_words.keys())[:top_words]
    dict_words = {}
    for count, word in enumerate(word_features):
        dict_words[word] = count
    return dict_words

def find_features(twit, dict_words):
    features = []
    for word in twit:
#         print(word)
        if word in dict_words:
#             print("test")
            features.append(dict_words[word])
    return features

# # Machine Learning 
# # CNN
# In[7]:

from keras.layers import MaxPooling1D, Conv1D, GlobalMaxPooling1D
# keras.layers.MaxPooling1D(pool_size=2, strides=None, padding='valid')
vocab_size = 2648

def text2features(X_raw):
    X_text = cleasing(X_raw)
    df = pd.DataFrame(data={"raw":X_raw, 'reduced':X_text, 'sentiment': y_raw})
    df.to_csv("data/clearning.csv")
    X_1, X_2 = ngrams_features(X_text)
    X_train = X_1
    dict_words = find_dict(X_train, vocab_size)
    X_train = [find_features(twit, dict_words) for twit in X_train]
    # save the binarizer, have not done that yet
    # save the dictionary, have not done that yet    
    return X_train, dict_words

X_train = list(text2features(X_raw)[0])

def discrete_sentiment(y):
    y_d = []
    for label in y:
        if label < -0.20:
            y_d.append(0)
        elif label < 0.25:
            y_d.append(1)
        else:
            y_d.append(2)
#     for label in y:
#         if label < -0:
#             y_d.append(-1)
#         else:
#             y_d.append(1)
    return y_d

y_train = discrete_sentiment(y_raw)
from collections import Counter
print("training labels:", Counter(y_train))

# In[8]:

y_encoded = np.zeros((len(y_train), 3),  dtype=int)
y_encoded[np.asarray(y_train) == 0, 0] = 1
y_encoded[np.asarray(y_train) == 1, 1] = 1
y_encoded[np.asarray(y_train) == 2, 2] = 1


# In[9]:
max_review_length = 14
num_test = 150
X_train = sequence.pad_sequences(X_train, maxlen=max_review_length)
X_test = X_train[:num_test]
X = X_train[num_test:]
y_test = y_encoded[:num_test]
y = y_encoded[num_test:]

# In[13]:

from keras.layers import Input, Dense, concatenate, Activation, Dropout
from keras.models import Model

from sklearn.metrics import confusion_matrix
from sklearn.metrics import precision_score

tweet_input = Input(shape=(max_review_length,), dtype='int32')
tweet_encoder = Embedding(vocab_size, 100, input_length=max_review_length)(tweet_input)

unigram_branch = Conv1D(filters=50, kernel_size=1, padding='valid', activation='relu', strides=1)(tweet_encoder)
unigram_branch = GlobalMaxPooling1D()(unigram_branch)

bigram_branch = Conv1D(filters=20, kernel_size=2, padding='valid', activation='relu', strides=1)(tweet_encoder)
bigram_branch = GlobalMaxPooling1D()(bigram_branch)

trigram_branch = Conv1D(filters=15, kernel_size=3, padding='valid', activation='relu', strides=1)(tweet_encoder)
trigram_branch = GlobalMaxPooling1D()(trigram_branch)

fourgram_branch = Conv1D(filters=10, kernel_size=4, padding='valid', activation='relu', strides=1)(tweet_encoder)
fourgram_branch = GlobalMaxPooling1D()(fourgram_branch)

fivegram_branch = Conv1D(filters=5, kernel_size=5, padding='valid', activation='relu', strides=1)(tweet_encoder)
fivegram_branch = GlobalMaxPooling1D()(fivegram_branch)

sixgram_branch = Conv1D(filters=5, kernel_size=6, padding='valid', activation='relu', strides=1)(tweet_encoder)
sixgram_branch = GlobalMaxPooling1D()(sixgram_branch)

merged = concatenate([
    unigram_branch, 
    bigram_branch, 
    trigram_branch, 
#    fourgram_branch, 
#    fivegram_branch, 
#    sixgram_branch
], axis=1)

merged = Dense(64, activation='relu')(merged)

merged = Dropout(0.85)(merged)

merged = Dense(3)(merged)

output = Activation('softmax')(merged)

model = Model(inputs=[tweet_input], outputs=[output])
model.compile(loss='categorical_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])
# =============================================================================

num_train = len(X_raw) - num_test
training_size = [
#        200, 
#        400, 
#        600, 
#        800, 
        num_train]
acc_list = []


for size in training_size:
    model.fit(X[:size], y[:size], batch_size=32, epochs=10, validation_split=0.1)
    y_predic = model.predict(X_test)
    predic = np.argmax(y_predic, 1)
    label = np.argmax(y_test, 1)

    results = (predic == label)
    accuracy = sum(results)/num_test
    print("test labels:", Counter(label))
    #print("train labels:", Counter(np.argmax(y, 1)))
    #print("all labels:", Counter(np.argmax(y_encoded, 1)))
    df_results = pd.DataFrame(data={"news":X_raw[:num_test], "label":label, "prediction":predic, "results":results})
    df_results.to_csv("results/sentiment_results_1031.csv")
    print("the accuracy for the testing data set is: ", accuracy)
    acc_list.append(accuracy)
    cm = confusion_matrix(label, predic)

# In[14]:




