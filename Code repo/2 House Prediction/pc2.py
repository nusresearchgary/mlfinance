#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 10 21:48:23 2018

@author: zhang
"""
import numpy as np
import pandas as pd
import time
import xgboost

train1_path ="hdb_train.csv"
train2_path = "private_train.csv"
feature1_dict_path = "hdb_data_dict.csv"
feature2_dict_path = "private_data_dict.csv"
test1_path = "hdb_test.csv"
test2_path = "private_test.csv"

t = time.time()
print("started to load the data")
hdb_raw = pd.read_csv(train1_path,header=0)
private_raw = pd.read_csv(train2_path,header=0)
hdb_feature_list_raw = pd.read_csv(feature1_dict_path,header=0)
private_feature_list_raw = pd.read_csv(feature2_dict_path,header=0)
hdb_test = pd.read_csv(test1_path,header=0)
private_test = pd.read_csv(test2_path,header=0)
print("data loading completed, ", time.time() - t)

######################################################
######  Utilities Functions
######################################################

def text2dict(inputs):
    dict_ = {}
    count = 1
    for i in range(len(inputs)):
        if inputs[i] not in dict_:
            dict_[inputs[i]] = count
            count+=1       
    return dict_
    
def dict_embedding(dict_, inputs):
    outputs = []
    for i in range(len(inputs)):
        outputs.append(dict_[inputs[i]])
    return outputs

def date2int(inputs):
    outputs = []
    min_date = min(inputs)
    min_date_int = 12 * int(min_date[0:4]) + int(min_date[5:len(min_date)])
    for i in range(len(inputs)):
        outputs.append(12 * int(inputs[i][0:4]) + int(inputs[i][5:len(inputs[i])]) - min_date_int)
    return outputs

def int2idx(inputs):
    min_date_int = min(inputs)
    outputs = []
    for i in range(len(inputs)):
        outputs.append(inputs[i] - min_date_int)
    return outputs


##################################
######## Model for HDB 
##################################
from sklearn import ensemble
from sklearn import datasets
from sklearn.utils import shuffle
from sklearn.metrics import mean_squared_error, mean_absolute_error
from datetime import datetime
from pytz import timezone

# #############################################################################
# Fit regression model
t = time.time()
print("training started")
fmt = "%Y-%m-%d %H:%M:%S %Z%z"
now_time = datetime.now(timezone('Asia/Singapore'))
print(now_time.strftime(fmt))

#params = {'n_estimators': 800, 'max_depth': 8, 'min_samples_split': 2,'learning_rate': 0.1, 'loss': 'ls'}
params = {'n_estimators': 800, 'max_depth': 8, 'min_samples_split': 2,'learning_rate': 0.2, 'loss': 'ls'}
clf = ensemble.GradientBoostingRegressor(**params)
clf.fit(X_train, np.log(Y_train))
Y_val_predic = np.exp(clf.predict(X_val))
mse = mean_squared_error(Y_val, Y_val_predic)
mae = mean_absolute_error(Y_val, Y_val_predic)
print("MSE: %.4f" % mse)
print("MAE", mae)
print("training has completed at ", (time.time()-t)//60)

#BEST IS 11,861
# #############################################################################