
# coding: utf-8

# In[18]:


import numpy as np
import pandas as pd
import time
import csv
import xgboost
from sklearn import ensemble
from sklearn import datasets
from sklearn.utils import shuffle
from sklearn.metrics import mean_squared_error, mean_absolute_error
from datetime import datetime
from pytz import timezone
import pickle
from sklearn.externals import joblib
#############################################################
## input and output file path are defined here
train1_path ="hdb_train.csv"
train2_path = "private_train.csv"
test1_path = "hdb_test.csv"
test2_path = "private_test.csv"
##############################################################
test_y_path = 'zhangge_a0122117x_PC2_play.csv'
##############################################################
## Data Loading
##############################################################
t = time.time()
print("=======================================================================")
print("===Started to load the training and testing data for HDB and Private House")
hdb_raw = pd.read_csv(train1_path,header=0)
private_raw = pd.read_csv(train2_path,header=0)
hdb_test = pd.read_csv(test1_path,header=0)
private_test = pd.read_csv(test2_path,header=0)
print("===Data loading completed in %.1f secs" %(time.time() - t))


# In[19]:


######################################################
######  Utilities Functions
######################################################
def text2dict(inputs):
    dict_ = {}
    count = 1
    for i in range(len(inputs)):
        if inputs[i] not in dict_:
            dict_[inputs[i]] = count
            count+=1       
    return dict_
    
def dict_embedding(dict_, inputs):
    outputs = []
    for i in range(len(inputs)):
        outputs.append(dict_[inputs[i]])
    return outputs

def date2int(inputs):
    outputs = []
    min_date = min(inputs)
    min_date_int = 12 * int(min_date[0:4]) + int(min_date[5:len(min_date)])
    for i in range(len(inputs)):
        outputs.append(12 * int(inputs[i][0:4]) + int(inputs[i][5:len(inputs[i])]) - min_date_int)
    return outputs

def int2idx(inputs):
    min_date_int = min(inputs)
    outputs = []
    for i in range(len(inputs)):
        outputs.append(inputs[i] - min_date_int)
    return outputs

def firstword(inputs):
    outputs = []
    for i in range(len(inputs)):
        outputs.append(inputs[i].partition(' ')[0])
    return outputs

def hdb_feature_engineering(inputs):
## feature engineering 
    t = time.time()
    f1 = inputs['flat_model']
    f1 = dict_embedding(text2dict(f1), f1)
    f2 = inputs['flat_type']  
    f2 = dict_embedding(text2dict(f2), f2)
    f3 = inputs['street_name']  
    f3 = dict_embedding(text2dict(f3), f3)
    f4 = inputs['town']  
    f4 = dict_embedding(text2dict(f4), f4)
    f5 = inputs['month']
    f5 = date2int(f5)
    f6 = inputs['lease_commence_date']
    f6 = int2idx(f6)
    f7 = inputs['floor_area_sqm']
    f8 = inputs['postal_code']
    f9 = inputs['floor']
    f10 = inputs['block']
    f10 = dict_embedding(text2dict(f10), f10)
    X = []
    X.append(f1)  # flat_model
    X.append(f2)  # flat_type
    #X.append(f3)  # street-name
    X.append(f4)  # town
    X.append(f5)  # month
    X.append(f6)  # lease commence date
    X.append(f7)  # floor_area_sqm
    X.append(f8)  # postal_code
    X.append(f9)  # floor
    #X.append(f10) # block
    X = np.asarray(X).T
    Y = []
    Y.append(inputs['resale_price'])
    Y = np.asarray(Y).T
    Y = np.reshape(Y, (Y.shape[0], ))
    return [X, Y]

def private_feature_engineering(inputs):
    t = time.time()
    inputs_p = inputs
    inputs_p[['postal_code']] = inputs_p[['postal_code']].replace(0, '-1')
    inputs_p[['floor_num', 'unit_num']] = inputs_p[['floor_num', 'unit_num']].replace(np.NaN, '-1')
    f1_p = inputs_p['project_name']
    f1_p = dict_embedding(text2dict(f1_p), f1_p)
    f2_p = inputs_p['address']
    f2_p = dict_embedding(text2dict(f2_p), f2_p)
    f3_p = inputs_p['floor_area_sqm']  
    f4_p = inputs_p['contract_date']
    f5_p = inputs_p['property_type']
    f5_p = dict_embedding(text2dict(f5_p), f5_p)
    f6_p = inputs_p['completion_date']
    f6_p = dict_embedding(text2dict(f6_p), f6_p)
    f7_p = inputs_p['type_of_sale']
    f7_p = dict_embedding(text2dict(f7_p), f7_p)
    f8_p = inputs_p['postal_district']
    f9_p = inputs_p['postal_sector']
    f10_p = inputs_p['postal_code']
    f11_p = inputs_p['region']
    f11_p = dict_embedding(text2dict(f11_p), f11_p)
    f12_p = inputs_p['area']
    f12_p = dict_embedding(text2dict(f12_p), f12_p)
    f13_p = inputs_p['month']
    f13_p = date2int(f13_p)
    f14_p = inputs_p['floor_num']
    f15_p = inputs_p['unit_num']
    f16_p = inputs_p['tenure']
    f16_p = firstword(f16_p)
    f16_p = dict_embedding(text2dict(f16_p), f16_p)
    f17_p = inputs_p['type_of_land']
    f17_p = dict_embedding(text2dict(f17_p), f17_p)
    X_p = []
    X_p.append(f1_p)  #project_name
    #X_p.append(f2_p) #address
    X_p.append(f3_p)  #floor_area_sqm
    #X_p.append(f4_p)  #contract_date
    X_p.append(f5_p) #property_type 
    #X_p.append(f6_p) #completion_date
    X_p.append(f7_p) #type_of_sales
    #X_p.append(f8_p) #postal_district
    #X_p.append(f9_p) #postal_sector
    X_p.append(f10_p)#postal_code
    #X_p.append(f11_p) #region
    #X_p.append(f12_p) #area
    X_p.append(f13_p) #month
    X_p.append(f14_p) #floor_num
    #X_p.append(f15_p) #unit_num
    X_p.append(f16_p) #tenure
    X_p.append(f17_p) #type_of_land
    X_p = np.asarray(X_p).T
    Y_p = []
    Y_p.append(inputs_p['price'])
    Y_p = np.asarray(Y_p).T
    Y_p = np.reshape(Y_p, (Y_p.shape[0], ))     
    return [X_p, Y_p]


# In[20]:


########################################################
### HDB Data Pre-Processing and Feature Engineering
########################################################
print("===HDB House: Data Preprocessing started for training and testing Data")
train_sample = len(hdb_raw)
train = hdb_raw
test = hdb_test
data = train.append(test, ignore_index=True)
X, Y = hdb_feature_engineering(data)
X_test=X[train_sample:]
X_, Y_ = X[:train_sample], Y[:train_sample]
idx = np.arange(train_sample)
np.random.shuffle(idx)
X_ = X_[idx,:]
Y_ = Y_[idx]
offset = int(0.9 * train_sample)
X_train = X_[:offset]
Y_train = Y_[:offset]
X_val = X_[offset:]
Y_val = Y_[offset:]
print("===HDB House: Data Preprocessing completed for training and testing data in %.1f mins" %((time.time() - t)/60))


# In[21]:


########################################################
### HDB Model Training
########################################################
t = time.time()
print("===HDB House Price Model: training started")
fmt = "===%Y-%m-%d %H:%M:%S %Z"
now_time = datetime.now(timezone('Asia/Singapore'))
print(now_time.strftime(fmt))
params1 = {'n_estimators': 1500, 'max_depth': 8, 'min_samples_split': 2,'learning_rate': 0.15, 'loss': 'ls'} #4.91, 11.6k
xgb = xgboost.XGBRegressor(**params1)
xgb.fit(X_train, np.log(Y_train))
Y_val_predic = np.exp(xgb.predict(X_val))
mse = mean_squared_error(Y_val, Y_val_predic)
mae = mean_absolute_error(Y_val, Y_val_predic)
print("===HDB MAE: %.1f" % mae)
print("===HDB House Price Model: training has completed in %.1f mins" %((time.time()-t)/60))


# In[22]:


########################################################
### HDB Model Prediction and Results Generation
########################################################
filename = 'zhangge_a0122117x_hdb_model.sav'
pickle.dump(xgb, open(filename, 'wb'))
print("===HDB House Price Forecasting model is saved at: ", filename)
loaded_model = joblib.load(filename)
Y_test_predic = np.exp(loaded_model.predict(X_test))
with open(test_y_path, 'w', newline='') as f:
    thewriter = csv.writer(f)
    thewriter.writerow(['index', 'price'])
    for i in range(Y_test_predic.shape[0]):
        thewriter.writerow([i, Y_test_predic[i]])
print("===Private House Price Prediction is completed, the prediction has been written to: ",test_y_path)


# In[24]:


####################################################################
# Private Housing: data Pre-processing and feature engineering
#####################################################################
print("===Private House: Data Pre-processing started for training and testing Data")
train = private_raw
test = private_test
data = train.append(test, ignore_index=True)
train_sample = len(train)
X, Y = private_feature_engineering(data)
X_test_p = X[train_sample:]
X_p, Y_p = X[:train_sample], Y[:train_sample]
idx = np.arange(train_sample)
np.random.shuffle(idx)
X_p = X_p[idx,:]
Y_p = Y_p[idx]
offset = int(X_p.shape[0] * 0.9)
X_train_p, Y_train_p = X_p[:offset], Y_p[:offset]
X_val_p, Y_val_p = X_p[offset:], Y_p[offset:]
print("===Private House: Data Preprocessing completed for training and testing data in %.1f mins" %((time.time() - t)/60))


# In[25]:


########################################################
######## Model for Private Housing
########################################################
print("===Private House Price Forecasting Model: Training started")
t = time.time()
fmt = "%Y-%m-%d %H:%M:%S %Z%z"
now_time = datetime.now(timezone('Asia/Singapore'))
print(now_time.strftime(fmt))
params2 = {'n_estimators': 1500, 'max_depth': 9, 'min_samples_split': 2,
           'learning_rate': 0.15, 'loss': 'ls'} #5.02, 845k
#xgb = xgboost.XGBRegressor(**params1)
xgb = xgboost.XGBRegressor(**params2)
xgb.fit(X_train_p, np.log(Y_train_p))
Y_val_p_predic = np.exp(xgb.predict(X_val_p))
mae_p = mean_absolute_error(Y_val_p, Y_val_p_predic)
print("===Private MAE is: %.1f" % mae_p)
print("===Private House Price Forecasting Model: training has completed in %.1f (mins) " %((time.time()-t)/60))
# #############################################################################


# In[26]:


##############################################################################
## Prediction
##############################################################################
filename = 'zhangge_a0122117x_private_model.sav'
pickle.dump(xgb, open(filename, 'wb'))
print("===Private House Price Forecasting model is saved at: ", filename)

loaded_model = joblib.load(filename)
Y_test_p_predic = np.exp(loaded_model.predict(X_test_p))
with open(test_y_path, 'a', newline='') as f:
    thewriter = csv.writer(f)
    for i in range(Y_test_p_predic.shape[0]):
        thewriter.writerow([i+len(hdb_test), Y_test_p_predic[i]])
print("===Private House Price Prediction is completed, the prediction has been appended to: ",test_y_path)
print("=======================================================================")

